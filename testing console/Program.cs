﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Dynamic;

namespace testing_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            Console.Write("Please enter your name: ");

            string name = Console.ReadLine();
            char letter = name[0];
            string word = name.Split(" ")[0];

            Console.WriteLine($"Hello {name}, your name is {name.Length} characters long and starts with a '{letter}'. First word: {word}");
        }
    }
}
